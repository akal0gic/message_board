<?

mb_internal_encoding('UTF-8');
setlocale(LC_ALL, 'ru_RU.UTF8');

/** Основные директории приложения
 * @property mixed $vendor          - директория с глобальными вендорами
 * @property mixed $incubator       - директория с инкубаторами фреймворка
 * 
 * @property mixed $projectRoot     - корневая директория проекта
 * 
 * @property mixed $appRoot         - директория активного приложени
 * @property mixed $appCache        - кеш активного приложения  */
class ProjectPaths {

  public $vendor;
  public $incubator;
  public $projectRoot;
  public $appRoot;
  public $appCache;

}

/** Основной класс инициализирующий доступные приложения.
 * @property ProjectPaths $path -           Основные директории проекта
 * @property mixed $appName - Название активного приложения
 * @property mixed $appEnv - Среда приложения может принимать значения (LOCAL, DEBUG, PRODUCTION)
 * @property mixed $appConfig - Конфиг приложения 
 */
class Project {

  public $path;
  public $domain;
  public $appName;
  public $appEnv;
  public $appConfig;

  function __construct() {
    # Выбор переменных среды на основе доменной зоны
    $this->domain = $_SERVER['SERVER_NAME']; # для корректной работы тестов именно так а не filter_input(INPUT_SERVER, 'SERVER_NAME')
    $this->appEnv = $this->getAppEnv();

    # Основные директории приложения
    $this->setAppPathes();

    # Загрузка конфига и инициализация приложения
    $this->initYii();

    $this->appConfig = $this->getConfig();
  }

  /** Устанавливает значение для одного или нескольких ключей
   * @param string|aray $keys -           Один или несколько ключей
   * @param mixed $value -                Любое скалярное значение. */
  private function defineHelper($keys, $value = NULL) {
    if (!is_array($keys)) $keys = [$keys];

    foreach ($keys as $key) {
      defined($key) or define($key, $value);
    }
  }

  /** Выбор переменных среды на основе доменной зоны */
  private function getAppEnv() {

    if (strpos($this->domain, '.loc')) {
      if ('cli' !== php_sapi_name() && !in_array(filter_input(INPUT_SERVER, 'REMOTE_ADDR'), ['127.0.0.1'])) {
        die('your ip address has been disallowed access');
      }

      error_reporting(E_ALL);
      ini_set('display_errors', 1);
      $this->defineHelper(['YII_DEBUG', 'APP_DEBUG'], true);
      $this->defineHelper(['YII_ENV', 'APP_ENV'], 'loc');

      $env = 'LOCAL';
    } else {
      error_reporting(E_ALL);
      ini_set('display_errors', 0);

      $this->defineHelper(['YII_DEBUG', 'APP_DEBUG'], false);
      $this->defineHelper(['YII_ENV', 'APP_ENV'], 'prod');

      $env = 'PRODUCTION';
    }

    return $env;
  }

  /** Устанавливает основные директории и неймспейсы для проекта и активного приложения */
  private function setAppPathes() {
    $this->path = new ProjectPaths();

    $project_root = str_replace('\\', '/', realpath(__DIR__ . '/../')) . '/';
    $www_root = str_replace('\\', '/', realpath(__DIR__ . '/../../')) . '/';

    $this->path->vendor = $project_root . '/vendor/';
    $this->path->incubator = $www_root . '_z_incubator/yii_incubator/';

    $this->path->projectRoot = $project_root;
    $this->path->appRoot = $this->path->projectRoot . 'app/';
    $this->path->appCache = $this->path->projectRoot . 'runtime/';
  }

  /** Инициализация фреймворка */
  private function initYii() {
    # composer
    require($this->path->vendor . 'autoload.php');
    # yii
    require($this->path->vendor . 'yiisoft/yii2/Yii.php');
    # incubator
    require($this->path->incubator . 'library/global.php');
    require($this->path->incubator . 'library/shortcuts.php');
    Yii::setAlias('@incubator', $this->path->incubator);
    # NS
    Yii::setAlias('@appRoot', $this->path->appRoot);
    Yii::setAlias('app', $this->path->appRoot);
    Yii::setAlias('@widgets', $this->path->appRoot . 'widgets/');
    Yii::setAlias('@public', $this->path->projectRoot . 'public/');
    Yii::setAlias('@tests', $this->path->projectRoot . 'tests/');
  }

  /** Загружает конфиг если он собран. Иначе собирает, сохраняет, загружает. */
  private function getConfig() {
    $env = require($this->path->appRoot . 'config/env/' . $this->appEnv . '.php');
    $config = require($this->path->appRoot . 'config/config.php');
    return $config;
  }

  /** Запускает фреймворк и вещает обработчики событий */
  function run() {
    $yii = new yii\web\Application($this->appConfig);
    Yii::setAlias('@bower', $this->path->vendor . 'bower-asset'); # redeclare default path
    $yii->on('beforeRequest', [$this, 'getModuleUrlRules']);
    $yii->run();
  }

  /** Добавляет роуты из модуля */
  public function getModuleUrlRules() {
    $moduleName = \incubator\helpers\ArrayHelper::getValue(explode('/', request()->getPathInfo()), 0, false);

    if ($moduleName && app()->hasModule($moduleName)) {
      $module = app()->getModule($moduleName);
      if (isset($module->urlRules)) app()->urlManager->addRules($module->urlRules);
    }
  }

}

# ********************************

/** @return Project  */
function Project() {
  static $project = NULL;
  if (!$project) $project = new Project();

  return $project;
}

if ('cli' !== php_sapi_name()) {
  Project()->run();
}

return Project();
# ********************************
