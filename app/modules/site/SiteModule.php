<?php

namespace app\modules\site;

class SiteModule extends \yii\base\Module {

  public $defaultRoute = 'site';
  public $controllerNamespace = 'app\modules\site\controllers';

}
