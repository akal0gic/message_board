<?php

namespace app\modules\site\controllers;

use yii\filters\AccessControl;

/** Основной контроллер сайта. Обслужевает служебные действия */

class SiteController extends \incubator\MVC\Controller {

  /** @inheritdoc */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                ['actions' => ['login', 'error', 'rpc'], 'allow' => TRUE],
            ],
        ]
    ];
  }

  /** <   Actions   > */ #region Actions <editor-fold>

  /** @inheritdoc */
  public function actions() {
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
    ];
  }

  #endregion </editor-fold>
}
