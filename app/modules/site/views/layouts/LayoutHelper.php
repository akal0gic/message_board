<?

/** Site layout helper */

namespace app\modules\site\views\layouts;

class LayoutHelper {

  /** Return site navigation link wrapped in li. Add class "active" to link of current url. */
  public static function top_navition_link($url, $text) {
    $isCurrentUrl = (\yii\helpers\Url::current() === $url);
    return '<li' . ($isCurrentUrl ? ' class="active"' : '') . '><a href="' . $url . '">' . $text . '</a></li>';
  }

}
