<?
/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\site\views\layouts\LayoutHelper;
?>

<? $this->beginPage(); ?>
<!doctype html>
<html>
  <head>
    <? $this->head() ?>
    <title> Сайтсофт </title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script src="/js/jquery-2.0.2.min.js"></script>
    <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>

  <? $this->beginBody(); ?>
  <body>

    <? /** navigation */ ?>
    <nav class="navbar navbar-default navbar-static-top">
      <div class="navbar-header">
        <a class="navbar-brand" href="/">Сайтсофт</a>
      </div>

      <div class="container-fluid"> 
        <ul class="nav navbar-nav">
          <?= LayoutHelper::top_navition_link('/', 'Главная') ?>
          <? if (user()->isGuest): ?>
            <?= LayoutHelper::top_navition_link('/user/login', 'Авторизация') ?>
            <?= LayoutHelper::top_navition_link('/user/register', 'Регистрация') ?>
          <? endif; ?>
        </ul>

        <ul class="nav navbar-nav pull-right">
          <? if (!user()->isGuest): ?>
            <li><a><?= user('login'); ?></a></li>
            <?= LayoutHelper::top_navition_link('/user/logout', 'Выход') ?>
          <? endif; ?>
        </ul>
      </div>
    </nav> 

    <? /** content */ ?>
    <div class="row-fluid">
      <div class="col-xs-offset-2 col-xs-8"><?= $content ?></div>
    </div>

    <script src="/js/bootstrap.min.js"></script>
    <? $this->endBody() ?>
  </body>
</html>
<? $this->endPage() ?>