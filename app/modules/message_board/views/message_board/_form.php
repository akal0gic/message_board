<?php

/** форма добавления нового сообщения */
/* @var $this \incubator\MVC\View */
/* @var $model \app\modules\message_board\models\Message */

# Хелперы вьюхи
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form_config = [
    'id' => 'message_board-form',
    'layout' => 'horizontal',
    'enableAjaxValidation' => false,
    'fieldConfig' => [
        'template' => "{input}{error}",
    ]
];
?>


<? $form = ActiveForm::begin($form_config); ?>

<div class="alert alert-danger">
  Сообщение не может быть пустым
</div>

<?= $form->field($model, 'message')->textarea(['placeholder' => $model->getAttributeLabel('message')]) ?>

<div class="form-group text-right">
  <?= Html::submitButton('Отправить сообщение', ['class' => 'btn btn-primary']) ?>
</div>

<? ActiveForm::end(); ?>






