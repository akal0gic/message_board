<?php
/** список сообщений и форма добавления нового  */
/* @var $this \incubator\MVC\View */
/* @var $model \app\modules\message_board\models\Message */
/* @var $message_list \app\modules\message_board\models\Message */
?>


<? /** форма отправки сообщения */ ?>
<? if (!user()->isGuest): ?>
  <?= $this->render('_form', ['model' => $model]); ?>
<? endif; ?>

<? /** список последних сообщений */ ?>
<div id="message_board_messages">
  <? foreach ($message_list as $message): ?>
    <div class="well">
      <h5><?= $message->user->login ?>:</h5>
      <?= $message->message ?> 
    </div>
  <? endforeach; ?>
</div>