<?php

namespace app\modules\message_board\controllers;

use yii\filters\AccessControl;
use app\modules\message_board\models\Message;

/** контроллер доскии сообщений. */
class MessageBoardСontroller extends \incubator\MVC\Controller {

  /** @inheritdoc */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                ['actions' => ['dashboard'], 'allow' => TRUE],
            ],
        ]
    ];
  }

  /** <   Actions   > */ #region Actions <editor-fold>

  /** Index */
  public function action_dashboard() {
    $model = new Message();

    if ($model->load(request()->post())) {
      $model->user_id = user('id');
      if ($model->save()) return $this->goHome();
    }

    $list = $model->find()->with('user')->orderBy('id desc')->limit(200)->all();
    return $this->render('dashboard', ['model' => $model, 'message_list' => $list]);
  }

  #endregion </editor-fold>
}
