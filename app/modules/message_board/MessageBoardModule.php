<?php

namespace app\modules\message_board;

class MessageBoardModule extends \yii\base\Module {

  public $controllerMap = [
      'message_board' => '\app\modules\message_board\controllers\MessageBoardСontroller'
  ];

  /** @return array Custom module url rules */
  public static function getUrlRules() {
    return [
        # CRUD тортов
        'GET /' => 'message_board/message_board/dashboard', # список + форма редактирования
        'POST /' => 'message_board/message_board/dashboard', # список + форма редактирования
    ];
  }

}
