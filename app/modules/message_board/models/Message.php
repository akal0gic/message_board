<?php

namespace app\modules\message_board\models;

use \incubator\db\ActiveRecord;
use \app\modules\user\models\User;

/** сообщения
 *
 * @property integer $id -          Первичный ключ
 * @property string $message -      Текст сообщения
 * @property string $user_id -      Id пользователя написавшего сообщение
 * 
 * 
 * Relations
 * @property \app\modules\user\models\User $user - User relation
 */
class Message extends ActiveRecord {

  public static $table_name = 'message';

  /** <   Relations   > */ #region Relations <editor-fold>
  /** User relations */
  public function getUser() {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  #endregion </editor-fold>

  /** @return array     Название атрибутов */
  public function attributeLabels() {
    return [
        'message' => 'Ваше сообщение...',
    ];
  }

  /** <   Validation   > */ #region Validation <editor-fold>
  /** Правила валидации  */
  public function rules() {
    return [
        [['message', 'user_id'], 'required', 'message' => 'Сообщение не может быть пустым'],
    ];
  }

  #endregion </editor-fold>
}
