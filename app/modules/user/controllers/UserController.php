<?php

namespace app\modules\user\controllers;

use yii\filters\AccessControl;
use app\modules\user\models\User;

/** Основной контроллер сайта. Обслужевает все служебные действия (вход, выход, отображение ошибок и т.д.)  */
class UserController extends \incubator\MVC\Controller {

  /** @inheritdoc */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                ['actions' => ['login', 'register'], 'allow' => true, 'roles' => ['?']],
                ['actions' => ['logout'], 'allow' => true, 'roles' => ['@']],
            ],
        ]
    ];
  }

  /** <   Actions   > */ #region Actions <editor-fold>

  /** Logout action */
  public function actionLogout() {
    user()->logout();
    return $this->goHome();
  }

  /** Login action */
  public function actionLogin() {
    $model = new User(['scenario' => 'login']);

    if ($model->load(request()->post()) AND $model->validate()) {
      $this->login($model->login, ($model->rememberMe ? (60 * 60 * 24 * 30) : 0));
      return $this->goBack();
    }

    return $this->render('login', ['model' => $model]);
  }

  /** Register action */
  public function actionRegister() {
    $model = new User(['scenario' => 'register']);

    if ($model->load(request()->post()) && $model->save()) {
      return $this->render('register_sucsess');
    }

    return $this->render('register', ['model' => $model]);
  }

  #endregion </editor-fold>

  /** <   Action Helpers   > */ #region Action Helpers <editor-fold>

  /** Авторизовывает пользователя по логину. */
  private function login($login, $duration = 0) {
    $identity = (new \app\modules\user\models\Identity())->findByLogin($login);
    user()->login($identity, $duration);
  }

  #endregion </editor-fold>
}
