<?php

namespace app\modules\user\models;

use \incubator\db\ActiveRecord;

/** пользователи сайта
 *
 * @property integer $id -          Первичный ключ
 * @property string $login -        Логин
 * @property string $password -     Пароль для новой модели или хеш пароля для ранее сохраненной модели
 */
class User extends ActiveRecord {

  public static $table_name = 'user';
  # дополнительные поля для форм
  public $passwordConfirm;
  public $rememberMe = true;

  /** @return array     Название атрибутов */
  public function attributeLabels() {
    return [
        'login' => 'Логин',
        'password' => 'Пароль',
        'passwordConfirm' => 'Повторите пароль',
        'rememberMe' => 'Запомнить меня'
    ];
  }

  /** <   Validation   > */ #region Validation <editor-fold>

  /** Доступные сценарии */
  public function scenarios() {
    return [
        'login' => ['login', 'password'],
        'register' => ['login', 'password', 'passwordConfirm'],
    ];
  }

  /** Правила валидации  */
  public function rules() {
    return [
        # регистрация
        ['login', 'unique', 'on' => 'register', 'message' => 'Пользователь с таким логином уже зарегистрирован в системе.'],
        [['login', 'password', 'passwordConfirm'], 'required', 'on' => 'register'],
        ['passwordConfirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают.', 'on' => 'register'],
        # авторизация
        [['login', 'password'], 'required', 'on' => ['login']],
        ['password', 'verifyPasswordOnLogin', 'on' => ['login']],
        ['rememberMe', 'boolean', 'on' => ['login']],
    ];
  }

  /** Расширенное правило валидации: Проверяет совпадает ли пароль с хешем пароля */
  public function verifyPasswordOnLogin($attribute, $params = null) {
    if ($this->hasErrors()) return false;

    $user = static::findByLogin($this->login);
    if (!$user || !password_verify($this->password, $user->password)) {
      $this->addError($attribute, 'Вход в систему с указанными данными невозможен');
    }
  }

  #endregion </editor-fold>

  /** @return bool    Действия перед сохранением */
  public function beforeSave($insert) {
    if (!parent::beforeSave($insert)) return false;

    # hash pasword
    if ($this->isNewRecord) {
      $this->password = password_hash($this->password, PASSWORD_DEFAULT);
      $this->auth_key = app()->security->generateRandomString();
    }

    return true;
  }

  /** Finds user by login
   * @param string $login
   * @return static */
  public static function findByLogin($login) {
    return static::findOne(['login' => $login]);
  }

  public function greet() {
    return 42;
  }

}
