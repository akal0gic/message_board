<?php

namespace app\modules\user\models;

class Identity extends User implements \yii\web\IdentityInterface {

  /** < IdentityInterface methods> */
  public function getAuthKey() {
    return $this->auth_key;
  }

  public function getId() {
    return $this->getPrimaryKey();
  }

  public function validateAuthKey($authKey) {
    return $this->getAuthKey() === $authKey;
  }

  public static function findIdentity($id) {
    return static::findOne($id);
  }

  public static function findIdentityByAccessToken($token, $type = null) {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

}
