<?php

/** страница регистрации на сайте */
/* @var $this \incubator\MVC\View */
/* @var $model app\modules\user\models\forms\RegisterForm */

# Хелперы вьюхи
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

# Параметры страницы
$this->title = 'Регистрация нового пользователя';

$form_config = [
    'id' => 'register-form',
    'layout' => 'horizontal',
    'enableAjaxValidation' => false,
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
    ]
];
?>

<? $form = ActiveForm::begin($form_config); ?>

<div class="form-group">
  <b>Регистрация</b>
</div>

<?=
        $form->field($model, 'login')->
        textInput(['autocomplete' => 'off', 'placeholder' => $model->getAttributeLabel('login')])
?>

<?=
        $form->field($model, 'password')->
        passwordInput(['placeholder' => $model->getAttributeLabel('password')])
?>

<?=
        $form->field($model, 'passwordConfirm')->
        passwordInput(['placeholder' => $model->getAttributeLabel('passwordConfirm')])
?>

<div class="form-group text-right">
  <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'submit-button']) ?>
</div>

<? ActiveForm::end(); ?> 








