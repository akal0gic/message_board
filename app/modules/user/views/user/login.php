<?php

/** страница авторизации на сайте */
/* @var $this \incubator\MVC\View */
/* @var $model app\modules\user\models\User */

# Хелперы вьюхи
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

# Параметры страницы
$form_config = [
    'id' => 'login-form',
    'layout' => 'horizontal',
    'enableAjaxValidation' => false,
    'fieldConfig' => [
        'template' => "{input}\n{error}",
    ]
];
?>

<? $form = ActiveForm::begin($form_config); ?>

<div class="form-group">
  <b>Авторизация</b>
</div>

<?= $form->field($model, 'login') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'rememberMe')->checkbox(['template' => "{input}\n{label}"])->label($model->getAttributeLabel('rememberMe')) ?>

<div class="form-group text-right">
  <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'submit-button']) ?>
</div>

<? ActiveForm::end(); ?>
