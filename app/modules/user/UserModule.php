<?php

namespace app\modules\user;

class UserModule extends \yii\base\Module {

  public $defaultRoute = 'user';
  public $controllerNamespace = 'app\modules\user\controllers';

  /** @return array Custom module url rules */
  public static function getUrlRules() {
    return [
        # регистрация
        'GET user/register' => 'user/user/register', # форма регистрации
        'POST user/register' => 'user/user/register', # регистрация нового пользователя
        # авторизация
        'GET user/login' => 'user/user/login', # форма авторизации
        'POST user/login' => 'user/user/login', # авторизации пользователя
        # logout
        'GET user/logout' => 'user/user/logout', # форма авторизации
    ];
  }

}
