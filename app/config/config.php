<?php

/* @var $this \Project */

$modules = [
    'site' => ['class' => 'app\modules\site\SiteModule'],
    'user' => ['class' => 'app\modules\user\UserModule'],
    'message_board' => ['class' => 'app\modules\message_board\MessageBoardModule'],
];

return [
    'id' => 'sitesoft',
    'language' => 'ru-RU',
    'vendorPath' => $this->path->vendor,
    'basePath' => $this->path->appRoot,
    'runtimePath' => $this->path->appCache,
    'controllerNamespace' => 'controllers',
    'bootstrap' => array_merge(['log'], $env['bootstrap']),
    'modules' => array_merge($modules, $env['modules']),
    'defaultRoute' => 'site/site/index',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => $env['db'],
        'log' => $env['log'],
        'view' => ['class' => 'incubator\MVC\View'],
        'request' => [
            'cookieValidationKey' => $env['components']['request']['cookieValidationKey']
        ],
        'urlManager' => [
            'cache' => false,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'class' => 'yii\web\UrlManager',
            'ruleConfig' => ['class' => 'incubator\web\UrlRule'],
            'rules' => [
                '/' => 'message_board/message_board/dashboard',
            ],
        ],
        'assetManager' => ['linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => false,
            ],
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\Identity',
            'enableAutoLogin' => true,
            'loginUrl' => ['/user/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/site/error',
        ],
    ],
];


