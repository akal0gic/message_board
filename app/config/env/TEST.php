<?php

/* @var $this \Project */

return [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'sqlite:' . $this->path->projectRoot . 'db/test.sqlite3',
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [['class' => 'yii\log\FileTarget', 'levels' => ['error', 'warning']]],
    ],
    'bootstrap' => ['debug', 'site', 'user'],
    'modules' => [
        'debug' => 'yii\debug\Module',
    ],
    'params' => [],
    'components' => [
        'request' => ['cookieValidationKey' => 'Праздник урожая во дворце труда!'],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ]
];
