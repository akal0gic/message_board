<?php

/* @var $this \Project */

return [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'sqlite:' . $this->path->projectRoot . 'db/development.sqlite3',
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [['class' => 'yii\log\FileTarget', 'levels' => ['error', 'warning']]],
    ],
    'bootstrap' => ['debug', 'gii'],
    'modules' => [
        'debug' => 'yii\debug\Module',
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['*'],
        ]
    ],
    'params' => [
    ],
    'components' => [
        'request' => ['cookieValidationKey' => 'Праздник урожая во дворце труда!'],
    ]
];
